package com.example.springminiproject.Respository;


import com.example.springminiproject.Model.Comment;
import com.example.springminiproject.Model.Post;
import com.example.springminiproject.Model.SubReddit;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface PostRepository {

    //select data from tb_subReddit
    @Select("select * from tb_reddit")
    @Result(property = "subreddit_id",column = "reddit_id")
    @Result(property = "subreddit_title",column = "reddit_title")
    List<SubReddit> getAllDataFromSubReddit();

    @Insert("INSERT INTO tb_reddit (reddit_id, reddit_title)" +
            "VALUES (#{id},#{title})")
    boolean InsertDataToSubReddite (int id,String title);
    //-------

    //select data from tb_post
    @Select("select * from tb_post")
    @Result(property = "subr", column = "redditp_id",one=@One(select = "getDataFromSubRedditById"))
    @Result(property = "image",column = "post_image")
    List<Post> getAllDataFromPost();

    @Select("select * from tb_reddit where reddit_id = #{id}")
    @Result(property = "subreddit_id",column = "reddit_id")
    @Result(property = "subreddit_title",column = "reddit_title")
    SubReddit getDataFromSubRedditById(int id);

    @Select("INSERT INTO tb_post(post_id, post_title, post_desc, redditp_id,vote,comment_size) " +
            "VALUES (#{id},#{title},#{desc},#{reddit_id},#{vote},#{comment_size})")
    void insertDataToPost(int id, String title, String desc, int reddit_id,int vote,int comment_size);

    @Select("select * from tb_post where post_id = #{post_id}")
    @Result(property = "subr", column = "redditp_id",one=@One(select = "getDataFromSubRedditById"))
    @Result(property = "image",column = "post_image")
    Post getDataFromPostById(int post_id);
    //-------

    //comment handler----
    @Select("select * from tb_comment")
    List<Comment> getAllComment();

    @Select("select * from tb_comment where post_id = #{post_id}")
    List<Comment> getAllCommentByPostId(int post_id);

    @Insert("insert into tb_comment (comment_id,comment_content, post_id) " +
            "values (#{id},#{content},#{post_id})")
    void insertDataToComment(int id,String content,int post_id);

    @Update("update tb_post set comment_size = #{sizec} where post_id = #{post_id}")
    int updateSizeCommentToPost(int sizec,int post_id);

    //vote Handler
    @Update("update tb_post set vote=#{vote} where post_id=#{post_id}")
    int updateVoteToPost(int vote,int post_id);

}
