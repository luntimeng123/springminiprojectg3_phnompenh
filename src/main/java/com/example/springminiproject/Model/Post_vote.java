package com.example.springminiproject.Model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Post_vote {
    private int vote_id;
    private int like;
    private int unlike;
}
