package com.example.springminiproject.Model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Post {
    private int post_id;
    private int subReddit_id;
    private String post_title;
    private String post_desc;
    private SubReddit subr;
    private int vote;
    private int comment_size;
    private String image;
}
