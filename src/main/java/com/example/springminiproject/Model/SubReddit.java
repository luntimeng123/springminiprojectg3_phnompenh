package com.example.springminiproject.Model;


import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SubReddit {
    private int subreddit_id;
    private String subreddit_title;
}
