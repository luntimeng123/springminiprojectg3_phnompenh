package com.example.springminiproject.Controller;


import com.example.springminiproject.Model.Comment;
import com.example.springminiproject.Model.Post;
import com.example.springminiproject.Model.Post_vote;
import com.example.springminiproject.Model.SubReddit;
import com.example.springminiproject.Service.PostService;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Controller
public class PostController {
    @Autowired
    private PostService postService;
    Post_vote post = new Post_vote();

    @GetMapping("/home")
    public String HomePage(Model model){
        model.addAttribute("listSubReddit",postService.getAllDataFromSubReddit());
        model.addAttribute("listPost",postService.getAllDataFromPost());
        //model.addAttribute("listComment",postService.getAllComment());
        model.addAttribute("like",post.getLike());
        model.addAttribute("unlike",post.getUnlike());
        return "index";
    }



    //SubReddit Handler-----
    @GetMapping("/addSub")
    public String AddSubReddit(){
        return "addSubReddit";
    }
    @PostMapping("/addSub/add")
    public String AddSubReddit(@ModelAttribute SubReddit subReddit){
        List<SubReddit> sub = postService.getAllDataFromSubReddit();
        subReddit.setSubreddit_id(sub.get(sub.size()-1).getSubreddit_id()+1);
        if (subReddit.getSubreddit_title() ==""){
            return "redirect:/addSub";
        }else {
            postService.InsertDataToSubReddite(subReddit.getSubreddit_id(), subReddit.getSubreddit_title());
        }
        return "redirect:/home";
    }



    //Post Hnadler
    @GetMapping("/Post")
    public String AddPostData(Model model){
        model.addAttribute("listReddit",postService.getAllDataFromSubReddit());
        return "addPost";
    }
    @PostMapping("/Post/add")
    public String AddPostData(@ModelAttribute Post post){
        int size = postService.getAllDataFromPost().size();
        int last_id;
        if (size != 0){
            last_id = postService.getAllDataFromPost().get(size-1).getPost_id();
        }else {
            last_id = 0;
        }
        if (post.getPost_title() != "" && post.getPost_desc() != "" && post.getSubReddit_id() != 0){
                postService.insertDataToPost(last_id+1,post.getPost_title(),post.getPost_desc(),post.getSubReddit_id(),0,0);
                //System.out.println(post.toString());
            return "redirect:/home";
        }else {
            return "redirect:/Post";
        }
    }
    @GetMapping("/Post/get/{id}")
    public String getDataFromPostById(@PathVariable int id,Model model){
        model.addAttribute("postObj",postService.getDataFromPostById(id));
        model.addAttribute("listcomment",postService.getAllCommentByPostId(id));
        model.addAttribute("sizeC",postService.getAllCommentByPostId(id).size());
        //System.out.println("commentLength: "+id+" "+postService.getAllCommentByPostId(id).size());
        return "postById";
    }





    //Comment Handler---
    @PostMapping("/add/comment/{id}")
    public String addComment(@ModelAttribute Comment comment,@PathVariable int id){

        int size = postService.getAllComment().size();
        int sizec = postService.getAllCommentByPostId(id).size();
        int last_id;
        if (size != 0){
            last_id = postService.getAllComment().get(size-1).getComment_id();
        }else {
            last_id = 0;
        }

        comment.setComment_id(last_id+1);
        comment.setPost_id(id);
        if(comment.getComment_content() != ""){
            postService.insertDataToComment(comment.getComment_id(),comment.getComment_content(),comment.getPost_id());
        }
        postService.updateSizeCommentToPost(sizec+1,id);
        System.out.println(sizec);

        return "redirect:/Post/get/{id}";
    }




    //voteHandler
    @GetMapping ("/update/plus_vote/{post_id}/{vote}")
    public String updateVoteToPostPlus(@PathVariable int vote,@PathVariable int post_id){
        vote = vote +1;
        postService.updateVoteToPost(vote,post_id);
        System.out.println("increase vote on Post: "+ vote);
        return "redirect:/home";
    }
    @GetMapping ("/update/minus_vote/{post_id}/{vote}")
    public String updateVoteToPostMinus(@PathVariable int vote,@PathVariable int post_id){
        vote = vote -1;
        postService.updateVoteToPost(vote,post_id);
        System.out.println("increase vote on Post: "+ vote);
        return "redirect:/home";
    }



}
