package com.example.springminiproject.Service;


import com.example.springminiproject.Model.Comment;
import com.example.springminiproject.Model.Post;
import com.example.springminiproject.Model.SubReddit;

import java.util.List;

public interface PostService {

    List<SubReddit> getAllDataFromSubReddit();
    boolean InsertDataToSubReddite (int id,String title);
    List<Post> getAllDataFromPost();
    void insertDataToPost (int id,String title,String desc,int reddit_id,int vote,int comment_size);
    Post getDataFromPostById(int post_id);
    List<Comment> getAllCommentByPostId(int post_id);
    void insertDataToComment(int id,String content,int post_id);
    List<Comment> getAllComment();
    int updateVoteToPost(int vote,int post_id);
    int updateSizeCommentToPost(int sizec,int post_id);
}
