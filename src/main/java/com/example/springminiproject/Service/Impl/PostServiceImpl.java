package com.example.springminiproject.Service.Impl;

import com.example.springminiproject.Model.Comment;
import com.example.springminiproject.Model.Post;
import com.example.springminiproject.Model.SubReddit;
import com.example.springminiproject.Respository.PostRepository;
import com.example.springminiproject.Service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Override
    public List<SubReddit> getAllDataFromSubReddit() {
        return postRepository.getAllDataFromSubReddit();
    }

    @Override
    public boolean InsertDataToSubReddite(int id, String title) {
        return postRepository.InsertDataToSubReddite(id,title);
    }

    @Override
    public List<Post> getAllDataFromPost() {
        return postRepository.getAllDataFromPost();
    }

    @Override
    public void insertDataToPost(int id, String title, String desc, int reddit_id,int vote,int comment_size) {
        postRepository.insertDataToPost(id,title,desc,reddit_id, vote,comment_size);
    }

    @Override
    public Post getDataFromPostById(int post_id) {
        return postRepository.getDataFromPostById(post_id);
    }

    @Override
    public List<Comment> getAllCommentByPostId(int post_id) {
        return postRepository.getAllCommentByPostId(post_id);
    }

    @Override
    public void insertDataToComment(int id, String content, int post_id) {
        postRepository.insertDataToComment(id,content,post_id);
    }

    @Override
    public List<Comment> getAllComment() {
        return postRepository.getAllComment();
    }

    @Override
    public int updateVoteToPost(int vote, int post_id) {
        postRepository.updateVoteToPost(vote,post_id);
        return post_id;
    }

    @Override
    public int updateSizeCommentToPost(int sizec, int post_id) {
        postRepository.updateSizeCommentToPost(sizec,post_id);
        return post_id;
    }

}
